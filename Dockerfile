FROM node:18.17.1-alpine@sha256:3482a20c97e401b56ac50ba8920cc7b5b2022bfc6aa7d4e4c231755770cf892f

COPY package.json yarn.lock /

RUN yarn install -g

WORKDIR /code
ENTRYPOINT ["/node_modules/.bin/tsc"]
